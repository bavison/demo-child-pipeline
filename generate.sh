#!/bin/bash

cat << EOF > generated-config.yml
stages:
  - childstage1
  - childstage2

first_job:
  stage: childstage1
  tags: [ cross ]
  script:
    - echo Hello world

second_job:
  stage: childstage2
  tags: [ cross ]
  script:
    - exit 0
EOF
